def printCombination(arr, n, r):

    data = [0] * r

    combinationUtil(arr, data, 0, n - 1, 0, r)



def combinationUtil(arr, data, start, end, index, r):
    if (index == r):
        for j in range(r):
            print(data[j], end=" ")
        print()
        return


    i = start;
    while (i <= end and end - i + 1 >= r - index):
        data[index] = arr[i];
        combinationUtil(arr, data, i + 1, end, index + 1, r)
        i += 1

def digits(number):
    K = 1
    res = []
    for idx in range(0, len(number), K):
        res.append(int(number[idx: idx + K]))

    return res


def permute(nums):
  result_perms = [[]]
  for n in nums:
    new_perms = []
    for perm in result_perms:
      for i in range(len(perm)+1):
        new_perms.append(perm[:i] + [n] + perm[i:])
        result_perms = new_perms
  return result_perms


def convert(list):
    s = [str(i) for i in list]
    res = int("".join(s))
    return (res)


if __name__ == "__main__":
    number = input()
    my_nums = digits(number)
    new_list = permute(my_nums)

    final_list = list()
    for i in new_list:
        final_list.append(convert(i))

    new_final_list = list()
    for i in final_list:
        if int(number) < i:
            new_final_list.append(i)

    try:
        print(min(new_final_list))
    except:
        print(0)
